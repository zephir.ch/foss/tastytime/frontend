FROM node:16-alpine

WORKDIR /app

COPY src/ .

RUN yarn

EXPOSE 8080

CMD yarn run build && yarn run start