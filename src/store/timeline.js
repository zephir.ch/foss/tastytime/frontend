export const state = () => ({
    list : [],
    active: null
})

export const getters = {
    totalDuration: (state, getters, rootState, rootGetters) => {
        let total = 0

        state.list.forEach(function(value) {
            total += value.duration
        })

        if (state.active) {
            total += (rootGetters['time/nowUnixTimestamp'] - state.active.updated_at)
        }

        return total
    },
    isCostunitActive: state => (costunit) => {
        return state.active && state.active.costunit_id == costunit.id
    }
}

export const mutations = {
    fill (state, data) {
        state.list = data;
    },
    activate (state, item) {
        state.active = null
        state.active = item;
    },
    reset (state) {
        state.active = null;
    }
}
