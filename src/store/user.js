export const state = () => ({
    user: false,
    isAuthorized: false
})

export const mutations = {
    login (state, user) {
        state.user = user
        state.isAuthorized = true
    }
}