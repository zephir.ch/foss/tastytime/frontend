import { formatDuration } from '~/plugins/time'

let interval = null

export const state = () => {
    return {
        now: new Date
    }
}

export const actions = {
    start ({ commit }) {
        interval = setInterval(() => {
            commit('updateTime')
        }, 1000)
    },
    stop () {
        clearInterval(interval)
    }
}

export const mutations = {
    updateTime (state) {
        state.now = new Date
    }
}

export const getters = {
    nowUnixTimestamp(state) {
        return Math.round(state.now.getTime() / 1000)
    },
    durationFormatted: (state, getters) => (sinceUnixTimestamp, showSeconds = false) => {
        return formatDuration(getters.nowUnixTimestamp - sinceUnixTimestamp, showSeconds)
    }
}