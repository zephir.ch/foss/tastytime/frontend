export const state = () => ({
    list : []
})

export const mutations = {
    fill (state, data) {
        state.list = data;
    }
}

export const actions = {
    async load ({ commit }) {
        this.$axios.get('/customers').then(response => {
            commit('fill', response.data);
        })
    }
} 