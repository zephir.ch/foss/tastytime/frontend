const 
    whitelister = require('purgecss-whitelister'),
    elementUiCssFiles = [
        'element-ui/lib/theme-chalk/base.css',
        'element-ui/lib/theme-chalk/icon.css',
        'element-ui/lib/theme-chalk/date-picker.css'
    ];

export default {
  env: {
    SOCKET_SERVER: process.env.SOCKET_SERVER || 'ws://websocket',
    baseUrl: process.env.API_URL || 'http://localhost:1234',
    basebrowserBaseURLUrl: process.env.API_URL_BROWSER || 'http://localhost:5678',
  },

  vue: {
    config: {
      performance: true
    }
  },

  mode: 'universal',
  server: {
    port: 8080, // default: 3000
    host: '0.0.0.0' // default: localhost
  },
  /*
  ** Headers of the page
  */
  head: {
    title: 'Tasty Time',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'apple-touch-icon', sizes: '152x152', href: '/apple-touch-icon.png' },
      { rel: 'mask-icon', color: '#553c9a', href: '/safari-pinned-tab.svg' },
      { name: 'theme-color', content: '#553c9a' }

    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#553c9a' },
  /*
  ** Global CSS
  */
  css: [
    '@/assets/css/custom.css',
    ...elementUiCssFiles
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
      '~/plugins/element',
      '~/plugins/moment',
      '~/plugins/time',
      {"src":"~/plugins/ws", ssr:false}
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
    '@nuxtjs/tailwindcss',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    ['@nuxtjs/pwa'],
    [
        'nuxt-fontawesome',
        {
            component: 'fa',
            imports: [
                {
                    set: '@fortawesome/free-solid-svg-icons',
                    icons: ['fas']
                },
                {
                    set: '@fortawesome/free-brands-svg-icons',
                    icons: ['faCreativeCommonsNc', 'faGitlab']
                }
            ]
        }
    ]
],
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  },
  purgeCSS: {
    whitelist: [...whitelister(elementUiCssFiles.map(item => 'node_modules/' + item))],
    whitelistPatterns: [/(^|\.)fa-/, /-fa($|\.)/, /(^|\.)hover:bg-/, /(^|\.)bg-/]
  },
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: { url: '/users/login', method: 'post', propertyName: 'jwt' },
          logout: { url: '/users/session', method: 'delete' },
          user: { url: '/users/session', method: 'get', propertyName: false }
        },
        tokenRequired: true,
        tokenType: 'Bearer'
      },
    },
    redirect: {
      login: '/login',
      logout: '/login',
      callback: '/login',
      home: '/'
    },
    token: {
      prefix: '_token.'
    },
    cookie: {
      prefix: 'auth.',
      options: {
        path: '/',
        expires: 90
      },
      
    }
  },
  pwa: {
    manifest: {
      name: 'Tasty Time',
      lang: 'en',
      "icons": [
        {
            "src": "/android-chrome-144x144.png",
            "sizes": "144x144",
            "type": "image/png"
        }
    ],
    "theme_color": "#603cba",
    "background_color": "#603cba",
    "display": "standalone"
    }
  },
  axios: {
    credentials: false,
    proxy: false,
    proxyHeaders: false,
  }
}
