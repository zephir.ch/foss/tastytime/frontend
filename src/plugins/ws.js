import Vue from 'vue'
import VueNativeSock from 'vue-native-websocket'

Vue.use(VueNativeSock, process.env.SOCKET_SERVER, { format: 'json' });