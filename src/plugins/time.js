import Vue from 'vue'

const formatDuration = (durationInSeconds, showSeconds = false) => {
    const duration = Vue.moment.duration(durationInSeconds, 'seconds')
    const hours = duration.hours()
    const minutes = duration.minutes()
    const seconds = showSeconds ? duration.seconds() : 0
    let formatted = ''

    if (hours !== 0) {
        formatted += `${hours} hour${hours === 1 ? '' : 's'}`
    }
    
    if (minutes !== 0) {
        if (hours !== 0) {
            formatted += ' '
        }

        formatted += `${minutes} minute${minutes === 1 ? '' : 's'}`
    } else if (seconds !== 0) {
        formatted = `${seconds} second${seconds === 1 ? '' : 's'}`
    } else if (hours === 0) {
        formatted = '-'
    }

    return formatted
}

Vue.prototype.$time = {
    formatDuration
}

export {
    formatDuration
}