import Vue from 'vue'
import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'

// Import the element-ui components you need,
// make sure to add the css files in nuxt config!
import { DatePicker } from 'element-ui'

locale.use(lang)

// Register the element-ui components
Vue.use(DatePicker);
