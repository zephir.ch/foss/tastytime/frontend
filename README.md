# Tasty Time Frontend

The nuxt frontend application image running SSR on **port 8080**.

## ENV VARS

|var|required|description
|---|--------|-----------
|`SOCKET_SERVER`|yes|The name of the socket server the application should connect to `wss://socket.xyz.com`.
|`API_URL`|yes|The api url the server should connect to. For example `https://api.xyz.com/admin`
|`API_URL_BROWSER`|yes|The api url the frontend (browser) client should connect to. For example `https://api.xyz.com/admin`